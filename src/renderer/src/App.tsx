import { BrowserRouter, Route, Routes } from 'react-router-dom'

import Dashboard from './screens/dashboard'
import STLRender from './screens/stl-render'
import Sobre from './screens/sobre'
// import Home from './screens/home'
import Loading from './screens/loading'
import XYZRender from './screens/point-cloud-render'

function App(): JSX.Element {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Dashboard />}>
          <Route index element={<XYZRender />} />
          <Route path="/sobre" element={<Sobre />} />
          <Route path="/preview" element={<STLRender />} />
          <Route path="/preview-xyz" element={<XYZRender />} />
          <Route path="/loading" element={<Loading />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
