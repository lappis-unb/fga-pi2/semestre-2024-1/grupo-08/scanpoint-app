import { RED, WHITE } from '../utils/colors'
import { StyledButtonProps } from '../types/styled-button'

const StyledButton = ({
  children,
  onClick,
  style,
  icon: Icon,
  id
}: StyledButtonProps): JSX.Element => {
  return (
    <button
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '12px',
        width: '150px',
        boxShadow: '0px',
        border: '0px',
        padding: '10px 20px',
        flexDirection: 'row',
        color: WHITE.principal,
        backgroundColor: style?.backgroundColor || RED.principal,
        ...style
      }}
      onClick={onClick}
      id={id}
    >
      <span style={{ marginLeft: Icon ? '10px' : '0' }}>{children}</span>
      {Icon && <Icon style={{ marginLeft: '10px' }} />} {}
    </button>
  )
}

export default StyledButton
