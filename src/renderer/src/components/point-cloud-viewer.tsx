import React, { useRef, useEffect } from 'react'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

interface Atom {
  x: number
  y: number
  z: number
  color?: number
}

interface XYZViewerProps {
  xyzData: Atom[] | null
}

const XYZViewer: React.FC<XYZViewerProps> = ({ xyzData }) => {
  const canvasRef = useRef<HTMLCanvasElement>(null)
  const sceneRef = useRef<THREE.Scene | null>(null)

  useEffect(() => {
    if (!xyzData || !canvasRef.current) return

    const scene = new THREE.Scene()
    const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)
    const renderer = new THREE.WebGLRenderer({ canvas: canvasRef.current })
    renderer.setSize(window.innerWidth / 3, window.innerHeight / 3)

    const controls = new OrbitControls(camera, renderer.domElement)

    // Add ambient light to the scene
    const ambientLight = new THREE.AmbientLight(0xffffff, 0.5)
    scene.add(ambientLight)

    // Add directional light to the scene
    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5)
    directionalLight.position.set(1, 1, 1).normalize()
    scene.add(directionalLight)

    // Create atoms
    xyzData.forEach((atom, index) => {
      const color = index % 2 === 0 ? 0xff0000 : 0x00ff00 // Example: alternating colors
      const geometry = new THREE.SphereGeometry(0.2, 32, 32)
      const material = new THREE.MeshPhongMaterial({ color })
      const sphere = new THREE.Mesh(geometry, material)
      sphere.position.set(atom.x, atom.y, atom.z)
      scene.add(sphere)
    })

    camera.position.z = 5

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const animate = () => {
      requestAnimationFrame(animate)
      controls.update()
      renderer.render(scene, camera)
    }
    animate()

    sceneRef.current = scene

    return () => {
      // Clean up Three.js scene
      if (sceneRef.current) {
        sceneRef.current.remove(...sceneRef.current.children)
      }
      renderer.dispose()
    }
  }, [xyzData])

  return <canvas ref={canvasRef} height={300} />
}

export default XYZViewer
