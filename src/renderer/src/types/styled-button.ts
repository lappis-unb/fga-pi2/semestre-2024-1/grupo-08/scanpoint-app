import { CSSProperties, ReactNode } from 'react'
import { IconType } from 'react-icons'

export interface StyledButtonProps {
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void
  children: ReactNode
  style?: CSSProperties
  icon?: IconType
  id?: string
}
