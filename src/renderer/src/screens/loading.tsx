import Patinho from '../assets/elemento2.png'
import StyledButton from '../components/styled-button'
import { IoIosCloseCircle } from 'react-icons/io'
import { useNavigate } from 'react-router-dom'
import { TbReload } from 'react-icons/tb'
import Spinner from '../assets/spinner.gif'
import { FaRegClock } from 'react-icons/fa'

const Loading = (): JSX.Element => {
  const navigate = useNavigate()
  const gotoNextPage = (): void => {
    navigate('/preview-xyz')
  }

  window.electron.ipcRenderer.on('redirect-to', (_, page) => {
    if (page === 'preview-xyz') {
      navigate('preview-xyz')
    }
  })

  return (
    <div style={styles.container}>
      <h1 style={styles.heading}>Escaneamento em andamento</h1>
      <div style={styles.content}>
        <img src={Patinho} alt="Elemento" style={styles.image} />
        <div style={styles.buttonContainer}>
          <StyledButton
            onClick={gotoNextPage}
            style={{ margin: '10px 0px' }}
            icon={IoIosCloseCircle}
          >
            Cancelar
          </StyledButton>
          <StyledButton
            onClick={gotoNextPage}
            style={{ backgroundColor: '#1F2E64' }}
            icon={TbReload}
          >
            Reiniciar
          </StyledButton>
        </div>
      </div>
      <img src={Spinner} alt="Carregando" style={styles.spinner} />
      <p style={styles.paragraph}>Tempo estimado para conclusão:</p>
      <div style={styles.timeContainer}>
        <FaRegClock style={{ color: 'black', marginRight: 5 }} />
        <p style={{ color: 'black' }}>30 min 10 seg</p>
      </div>
    </div>
  )
}

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column' as const,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    padding: '0 20px'
  },
  timeContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 30
  },
  paragraph: {
    marginBottom: 0,
    color: 'black',
    fontWeight: 500
  },
  heading: {
    color: 'black',
    marginBottom: 10,
    textAlign: 'center' as const
  },
  content: {
    display: 'flex'
  },
  image: {
    width: 'auto',
    height: '250px'
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'column' as const,
    marginLeft: 20
  },
  spinner: {
    width: '90px',
    height: '90px'
  }
}

export default Loading
