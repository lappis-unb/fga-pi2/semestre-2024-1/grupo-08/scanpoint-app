const Sobre = (): JSX.Element => {
  return (
    <div style={styles.container}>
      <div style={styles.card}>
        <h1 style={styles.heading}>O que é o ScanPoint?</h1>
        <p style={styles.paragraph}>
          O ScanPoint é uma solução para escaneamento tridimensional de objetos físicos. Originou-se
          da colaboração entre um professor e um aluno e foi desenvolvido como um projeto para ser
          realizado na disciplina de Projeto Integrador 2, envolvendo estudantes dos cursos de
          Engenharia de Software, Engenharia Eletrônica, Engenharia Aeroespacial, Engenharia de
          Energia e Engenharia Automotiva
        </p>
        <p style={styles.paragraph}>
          O ScanPoint não apenas proporciona uma experiência intuitiva e fácil de usar para os
          usuários, mas também oferece uma oportunidade única para os estudantes aplicarem seus
          conhecimentos teóricos na prática, colaborando em um projeto real e relevante para o
          mercado.
        </p>
      </div>
      <div style={styles.card}>
        <h1 style={styles.heading}>Quem somos nós?</h1>
        <p style={styles.paragraph}>
          Como parte do time de desenvolvimento do ScanPoint temos quinze estudantes de engenharia.
          Como orientadores e avaliadores, temos cinco professores da Universidade de Brasília. O
          gitlab de cada um dos membros pode ser acessado pelo link da página:
        </p>
        <a href="https://communication-box-fga-pi2-semestre-2024-1-grupo--f7dec8a78ed749.gitlab.io/">
          https://communication-box-fga-pi2-semestre-2024-1-grupo--f7dec8a78ed749.gitlab.io/
        </a>
        <p style={styles.paragraph}>
          E as demais informações sobre os docentes e a disciplina de Projeto integrador 2 podem ser
          encontradas no link:
        </p>
        <a href="https://pi2.lappis.rocks/">https://pi2.lappis.rocks/</a>
      </div>
    </div>
  )
}

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row' as const,
    justifyContent: 'center',
    alignItems: 'stretch', // Permite que os cards tenham a mesma altura
    textAlign: 'center' as const,
    height: '100vh',
    padding: '0 20px',
    gap: '20px' // Espaçamento entre os cards
  },
  card: {
    marginTop: '70px',
    flex: 1,
    height: '78%',
    display: 'flex',
    flexDirection: 'column' as const,
    justifyContent: 'flex-start',
    padding: '20px',
    boxSizing: 'border-box' as const,
    border: '1px solid #ccc',
    borderRadius: '10px',
    boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
    backgroundColor: 'white'
  },
  heading: {
    color: 'black',
    marginBottom: '20px'
  },
  paragraph: {
    marginBottom: '20px',
    color: 'black'
  },
  link: {
    color: '#007BFF',
    textDecoration: 'none',
    wordBreak: 'break-word', // Garante que links longos sejam quebrados em linhas
    marginBottom: '20px'
  }
}

export default Sobre
