import { useNavigate } from 'react-router-dom'
import { FaGreaterThan } from 'react-icons/fa'
import { useCallback } from 'react'

import StyledButton from '../components/styled-button'
import Elemento from '../assets/elemento1.png'

const Home = (): JSX.Element => {
  const navigate = useNavigate()
  const gotoNextPage = useCallback(() => {
    window.api.send('start-scan')
  }, [])

  window.electron.ipcRenderer.on('redirect-to', (_, page) => {
    if (page === 'load') {
      navigate('loading')
    }
  })

  return (
    <div style={styles.container}>
      <h1 style={styles.heading}>Seja bem vindo(a)! Vamos começar?</h1>
      <p style={styles.paragraph}>
        Apresentamos o ScanPoint, uma solução completa para escaneamento e reprodução 3D de objetos
        físicos. Após posicionar o objeto no centro do suporte, com apenas um clique em Iniciar,
        você poderá facilmente digitalizar objetos do mundo real, acompanhar o progresso em tempo
        real e pré-visualizar o resultado final do modelo 3D gerado. Após o escaneamento, será
        possível baixar o arquivo 3D e utilizá-lo em sua impressora 3D!
      </p>
      <StyledButton onClick={gotoNextPage} icon={FaGreaterThan}>
        Iniciar
      </StyledButton>
      <img src={Elemento} alt="Elemento" style={styles.image} />
    </div>
  )
}

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column' as const,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center' as const,
    height: '100vh',
    padding: '0 20px'
  },
  heading: {
    color: 'black',
    marginBottom: 50
  },
  paragraph: {
    marginBottom: 30,
    color: 'black'
  },
  image: {
    marginBottom: 30,
    width: 'auto',
    height: '200px'
  }
}

export default Home
