import { Outlet } from 'react-router-dom'
import { Link } from 'react-router-dom'

import { RED, WHITE } from '../utils/colors'
import Logo from '../assets/logo.png'

const Dashboard = (): JSX.Element => {
  return (
    <>
      <header
        style={{
          backgroundColor: RED.principal,
          height: '15vh',
          minHeight: '100px',
          width: '100vw',
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: '0 80px',
          alignItems: 'center',
          flexWrap: 'wrap'
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center'
          }}
        >
          <div
            style={{
              backgroundColor: WHITE.principal,
              width: '100px',
              height: '97px',
              borderRadius: '10px',
              //top rigth bottom left
              padding: '3px 18px 15px 18px',
              marginRight: '20px'
            }}
          >
            <Link to="/" style={{ textDecoration: 'none', color: WHITE.principal }}>
              <img
                alt="logo"
                className="logo"
                style={{
                  width: '100%',
                  height: '100%'
                }}
                src={Logo}
              />
            </Link>
          </div>
          <Link to="/" style={{ textDecoration: 'none', color: WHITE.principal }}>
            <h1 style={{ margin: 0 }}>ScanPoint</h1>
          </Link>
        </div>
        <Link to="/sobre" style={{ textDecoration: 'none', color: WHITE.principal }}>
          <h2 style={{ margin: 0 }}>Sobre</h2>
        </Link>
      </header>
      <main
        style={{
          backgroundColor: WHITE.principal,
          height: '85vh',
          width: '100%',
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        <Outlet />
      </main>
    </>
  )
}

export default Dashboard
