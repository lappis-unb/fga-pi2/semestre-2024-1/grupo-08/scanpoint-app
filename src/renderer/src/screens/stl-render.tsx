/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { StlViewer } from 'react-stl-viewer'
import greenCheckVector from '../assets/green-check.svg'
import { StyledButton } from '@renderer/components'
import { GREEN } from '../utils/colors'
import { FaDownload, FaSignOutAlt } from 'react-icons/fa'

const STLRender = (): JSX.Element => {
  const closeApp = (e): void => {
    e.preventDefault()
    window.electron.ipcRenderer.send('close')
  }

  const handleClick = (e) => {
    e.preventDefault()
    window.electron.ipcRenderer.send('runDownloadFile')
  }

  const handleTestConvertSTL = (e) => {
    e.preventDefault()
    window.electron.ipcRenderer.send('convertStlFile')
  }

  return (
    <div
      style={{
        height: '100%',
        width: '50vw',
        minWidth: '400px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center',
        color: 'black'
      }}
    >
      <h1 style={{ marginTop: 40, marginBottom: 25 }}>
        Escaneamento finalizado!
        <img style={{ marginLeft: 10 }} src={greenCheckVector} alt="checked" />
      </h1>
      <StlViewer
        style={{
          height: '50vh',
          minWidth: '400px',
          width: '40vw',
          top: 0,
          left: 0,
          backgroundColor: 'black'
        }}
        orbitControls
        shadows
        controls
        url="./src/assets/cube.stl"
      />
      <h3
        style={{
          marginTop: 20,
          marginBottom: 20,
          wordBreak: 'break-word',
          wordWrap: 'break-word'
        }}
      >
        {' '}
        Aqui está uma prévia do seu objeto! Caso esteja satisfeito, você pode realizar o download
        através do botão “baixar”. Caso deseje refazer o processo, por favor clique em “re-iniciar”.
      </h3>

      <div
        style={{
          display: 'flex',
          width: '450px',
          flexWrap: 'wrap',
          justifyContent: 'space-between',
          marginBottom: 80
        }}
      >
        <StyledButton
          icon={FaDownload}
          style={{ width: 140, backgroundColor: GREEN.principal }}
          onClick={handleTestConvertSTL}
        >
          Teste de conversão STL
        </StyledButton>
        <StyledButton
          icon={FaDownload}
          style={{ width: 140, backgroundColor: GREEN.principal }}
          onClick={handleClick}
        >
          Baixar
        </StyledButton>
        <StyledButton onClick={closeApp} icon={FaSignOutAlt} style={{ width: 140 }}>
          Sair
        </StyledButton>
      </div>
    </div>
  )
}

export default STLRender
