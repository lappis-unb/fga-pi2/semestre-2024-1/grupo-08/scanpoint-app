import { useEffect, useState } from 'react'
import XYZViewer from '../components/point-cloud-viewer'
import greenCheckVector from '../assets/green-check.svg'
import { StyledButton } from '@renderer/components'
import { FaArrowRight } from 'react-icons/fa'
import { GREEN } from '@renderer/utils/colors'
import { useNavigate } from 'react-router-dom'

interface Atom {
  x: number
  y: number
  z: number
  color?: number
}

const XYZRender = (): JSX.Element => {
  const [xyzData, setXYZData] = useState<Atom[] | null>(null)

  useEffect(() => {
    // Fetch your .xyz file here
    if (!xyzData) {
      fetch('./src/assets/output.xyz')
        .then((response) => response.text())
        .then((contents) => {
          const parsedData = parseXYZ(contents)
          console.log(parsedData)
          setXYZData(parsedData)
        })
        .catch((error) => {
          console.error('Error fetching XYZ file:', error)
        })
    }
  }, [xyzData])

  const parseXYZ = (contents: string): Atom[] => {
    const lines = contents.split('\n')
    const numAtoms = lines.length - 1
    console.log(numAtoms)
    const data: Atom[] = []

    // Start parsing from the second line (index 1)
    for (let i = 0; i <= numAtoms; i++) {
      const [x, y, z] = lines[i].trim().split(/\s+/).map(parseFloat)
      data.push({ x, y, z })
    }

    return data
  }

  const navigate = useNavigate()
  const gotoNextPage = (): void => {
    navigate('/preview')
  }

  return (
    <div
      style={{
        height: '100%',
        width: '50vw',
        minWidth: '400px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center',
        color: 'black'
      }}
    >
      <h1 style={{ marginTop: 40, marginBottom: 25 }}>
        Escaneamento finalizado!
        <img style={{ marginLeft: 10 }} src={greenCheckVector} alt="checked" />
      </h1>
      <div>{xyzData ? <XYZViewer xyzData={xyzData} /> : <p>Loading XYZ data...</p>}</div>
      <h3
        style={{
          marginTop: 20,
          marginBottom: 20,
          wordBreak: 'break-word',
          wordWrap: 'break-word'
        }}
      >
        {' '}
        Acima se encontra a nuvem de pontos, se o resultado estiver satisfatório, clique em avançar.
      </h3>
      <div
        style={{
          display: 'flex',
          width: '450px',
          flexWrap: 'wrap',
          justifyContent: 'space-between',
          marginBottom: 80
        }}
      >
        <StyledButton
          icon={FaArrowRight}
          style={{ width: 140, backgroundColor: GREEN.principal }}
          onClick={gotoNextPage}
        >
          Avançar
        </StyledButton>
      </div>
    </div>
  )
}

export default XYZRender
