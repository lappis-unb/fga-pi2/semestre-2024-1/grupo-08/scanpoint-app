const RED = {
  principal: '#C11C44'
}
const GREEN = {
  principal: '#477138'
}
const BLUE = {
  principal: '#1F2E64'
}
const PINK = {
  principal: '#BD8390'
}
const GRAY = {
  principal: '#818181',
  800: '#6C6C6C'
}
const BROWN = {
  principal: '#444444'
}
const BLACK = {
  principal: '#000000'
}
const WHITE = {
  principal: '#FFFFFF'
}

export { RED, GREEN, BLUE, PINK, GRAY, BROWN, BLACK, WHITE }
