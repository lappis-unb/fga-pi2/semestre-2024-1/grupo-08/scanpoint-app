import { app, shell, BrowserWindow, ipcMain, dialog } from 'electron'
import path, { join } from 'path'
import { electronApp, optimizer, is } from '@electron-toolkit/utils'
import { PythonShell } from 'python-shell'
import icon from '../../resources/icon.png?asset'
import { ArduinoService } from './arduinoService'
import { appendFileSync } from 'fs'

let arduinoService: ArduinoService

function showErrorDialog(errorMessage: string): void {
  dialog.showErrorBox('Error', errorMessage)
}

function createWindow(): void {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 900,
    height: 670,
    show: false,
    autoHideMenuBar: true,
    ...(process.platform === 'linux' ? { icon } : {}),
    webPreferences: {
      preload: join(__dirname, '../preload/index.js'),
      sandbox: false,
      nodeIntegration: true,
      contextIsolation: false
    }
  })

  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
  })

  mainWindow.webContents.setWindowOpenHandler((details) => {
    shell.openExternal(details.url)
    return { action: 'deny' }
  })

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
  if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
    mainWindow.loadURL(process.env['ELECTRON_RENDERER_URL'])
  } else {
    mainWindow.loadFile(join(__dirname, '../renderer/index.html'))
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  // Set app user model id for windows
  electronApp.setAppUserModelId('com.electron')

  // Default open or close DevTools by F12 in development
  // and ignore CommandOrControl + R in production.
  // see https://github.com/alex8088/electron-toolkit/tree/master/packages/utils
  app.on('browser-window-created', (_, window) => {
    optimizer.watchWindowShortcuts(window)
  })

  // IPC test
  ipcMain.on('ping', () => console.log('pong'))

  createWindow()

  arduinoService = ArduinoService.Instance({
    onDataHandler: async (data) => {
      const stringData = data?.toString()
      if (stringData === arduinoService.responseArduino.SCANNING) {
        const mainWindow = BrowserWindow.getAllWindows()[0]
        mainWindow.webContents.send('redirect-to', 'load')
      }

      if (
        data &&
        (!(stringData as string)!.match(/^A_.*/) ||
          stringData === arduinoService.responseArduino.FINISHED)
      ) {
        if (stringData === arduinoService.responseArduino.FINISHED) {
          ipcMain.emit('convertStlFile')
          return
        }
        const outputFile = path.resolve(__dirname, '../../data', 'output.txt')
        appendFileSync(outputFile, data.toString() + '\n')
      }
    }
  })

  // IPC to start the scanning process
  ipcMain.on('start-scan', async () => {
    try {
      arduinoService.startScan()
    } catch (error) {
      dialog.showErrorBox('Erro ao começar scan', `Erro ao começar scanner: ${error}`)
    }
  })
  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

ipcMain.on('close', () => {
  app.quit()
})

ipcMain.on('runDownloadFile', async () => {
  try {
    PythonShell.run('./scripts/download-file.py').then((messages) => {
      console.log(messages)
      console.log('finished')
    })
  } catch (err) {
    if (err instanceof Error) {
      showErrorDialog('Error running download file script: ' + err.message)
    } else {
      showErrorDialog('An unknown error occurred while running the download file script')
    }
  }
})

ipcMain.on('convertStlFile', async () => {
  try {
    PythonShell.run('./scripts/convertToPointCloud.py')
      .then(() => {
        PythonShell.run('./scripts/converter-stl.py')
          .then(() => {
            const mainWindow = BrowserWindow.getAllWindows()[0]
            mainWindow.webContents.send('redirect-to', 'preview-xyz')
          })
          .catch(() => {
            dialog.showErrorBox(
              'Erro ao fazer stl',
              'Falha ao converter dados em stl, tente novamente'
            )
          })
      })
      .catch(() => {
        dialog.showErrorBox(
          'Erro ao fazer nuvem de pontos',
          'Falha ao converter dados em nuvem de pontos, tente novamente'
        )
      })

    // If error
  } catch (err) {
    if (err instanceof Error) {
      showErrorDialog('Error running STL file conversion script: ' + err.message)
    } else {
      showErrorDialog('An unknown error occurred while running the STL file conversion script')
    }
  }
})

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
