import { SerialPort, ReadlineParser } from 'serialport'

import { platform } from '@electron-toolkit/utils'

type ConstructorProps = {
  onOpenHandler?: () => void
  onCloseHandler?: () => void
  onDataHandler?: (data: unknown) => void
  onErrorHandler?: (err: unknown) => void
}

type ListenerEvents = 'close' | 'drain' | 'error' | 'finish' | 'pipe' | 'unpipe' | string | symbol
type StatusToCheck =
  | 'isMotorDefective'
  | 'isSensorDefective'
  | 'isRunningTests'
  | 'isPreparing'
  | 'isScanning'
  | 'isStopped'

export class ArduinoService {
  private static _instance: ArduinoService
  private port: SerialPort
  private parser: ReadlineParser
  private eventsWatched: string[] = []
  private emptyStream = true

  static WINDOWS_SERIAL_PATH = '/dev/ttyACM0'
  // static LINUX_SERIAL_PATH = '/dev/ttyUSB0'
  static LINUX_SERIAL_PATH = '/dev/ttyACM0'
  static MACOS_SERIAL_PATH = '/dev/ttyUSB0'

  // Redundant states of arduino
  private isMotorDefective = false
  private isSensorDefective = false
  private isRunningTests = false
  private isPreparing = false
  private isScanning = false
  private isStopped = false

  public readonly commandArduino = {
    STOP: 'C_STOP',
    RESTART: 'C_RESTART',
    SCAN: 'C_SCAN',
    TEST: 'C_TEST',
    STATUS: 'C_STATUS',
    CANCEL: 'C_CANCEL'
  }

  public readonly responseArduino = {
    TESTING: 'A_TEST',
    SCANNING: 'A_SCAN',
    RESTARTING: 'A_RESTART',
    READY: 'A_READY',
    ERROR_SENSOR: 'A_ERROR_SENSOR',
    ERROR_MOTOR: 'A_ERROR_MOTOR',
    STOPPED: 'A_STOP',
    FINISHED: 'A_FINISHED'
  }

  private constructor({
    onCloseHandler,
    onDataHandler,
    onErrorHandler,
    onOpenHandler
  }: ConstructorProps) {
    let serialPath = ArduinoService.LINUX_SERIAL_PATH

    if (platform.isMacOS) {
      serialPath = ArduinoService.MACOS_SERIAL_PATH
    }

    if (platform.isWindows) {
      serialPath = ArduinoService.WINDOWS_SERIAL_PATH
    }

    this.port = new SerialPort({ path: serialPath!, baudRate: 9600 })
    this.parser = this.port.pipe(new ReadlineParser({ delimiter: '\r\n' }))

    if (onOpenHandler) {
      this.addListener('open', () => {
        this.resetStatus()
        this.sendCommandToArduino(this.commandArduino.TEST)
        onOpenHandler()
      })
    }

    if (onCloseHandler) {
      this.addListener('close', () => {
        this.resetStatus()
        onCloseHandler()
      })
    }

    if (onDataHandler) {
      this.addListener('data', (data) => {
        this.recieveResponseFromArduino((data as string).toString())
        onDataHandler(data)
      })
    }

    if (onErrorHandler) {
      this.addListener('error', onErrorHandler)
    }
  }

  public static Instance(constructorProps: ConstructorProps): ArduinoService {
    return this._instance || (this._instance = new this(constructorProps))
  }

  public addListener(event: ListenerEvents, eventHandler: (any: unknown) => void): void {
    if (!event || !eventHandler) {
      return
    }

    const listeningEvent = this.eventsWatched.find((i) => i === event)
    if (listeningEvent) {
      return
    }
    console.log(`Bind event:"${event.toString()}"`)
    this.eventsWatched.push(event.toString())
    if (event === 'data') {
      this.parser.on('data', eventHandler)
      return
    }
    this.port.addListener(event, eventHandler)
  }

  public removeListener(event: ListenerEvents, callback: (any: unknown) => unknown): void {
    if (!event) {
      return
    }

    const listeningEvent = this.eventsWatched.find((i) => i === event)
    if (!listeningEvent) {
      return
    }

    this.eventsWatched = this.eventsWatched.filter((i) => i === event.toString())

    console.log(`Removed bind, event: "${event.toString()}"`)
    if (event === 'data') {
      this.parser.removeListener('data', callback)
      return
    }
    this.port.removeListener(event, callback)
  }

  public removeAllListeners(): void {
    this.eventsWatched = []
    this.port.removeAllListeners()
    this.parser.removeAllListeners()
    console.log('Removed all listeners')
  }

  public isOpen = (): boolean => this.port.isOpen

  public open(): void {
    if (this.port.isOpen) {
      return
    }
    this.port.open((err) => {
      if (err) {
        throw new Error('Falha ao abrir a porta verifique se o cabo está conectado corretamente')
      }
    })
  }

  public close(): void {
    if (!this.port.isOpen) {
      this.port.flush()
      return
    }
    this.port.close((err) => {
      if (err) {
        throw new Error('Falha ao abrir a porta verifique se o cabo está conectado corretamente')
      }
      this.port.flush()
    })
  }

  public write(chunck: unknown): void {
    if (!this.port.isOpen) {
      throw new Error('Porta não conectada')
    }
    if (!this.emptyStream) {
      throw new Error('Scanner ainda não recebeu o commando, espere um pouco por favor')
    }
    const tempStreamStatus = this.emptyStream
    this.emptyStream = false
    const emptyStream = this.port.write(chunck, (err) => {
      if (err) {
        if (tempStreamStatus && !this.emptyStream) {
          this.emptyStream = true
        }
        console.log('err', err)

        throw new Error('Falha ao mandar comando para o scanner')
      }
    })
    this.emptyStream = emptyStream
  }

  private sendCommandToArduino(commandToSend: string): void {
    if (!commandToSend) {
      return
    }
    switch (commandToSend) {
      case this.commandArduino.STOP:
        return this.write(this.commandArduino.STOP)
      case this.commandArduino.SCAN:
        this.checkBlockingStatus([
          'isMotorDefective',
          'isSensorDefective',
          'isScanning',
          'isRunningTests',
          'isStopped',
          'isPreparing'
        ])
        return this.write(this.commandArduino.SCAN)
      case this.commandArduino.TEST:
        this.checkBlockingStatus(['isScanning', 'isRunningTests', 'isPreparing'])
        this.write(this.commandArduino.TEST)
        break
      case this.commandArduino.CANCEL:
        this.checkBlockingStatus(['isRunningTests', 'isStopped', 'isPreparing'])
        this.write(this.commandArduino.CANCEL)
        break
      case this.commandArduino.RESTART:
        this.checkBlockingStatus([
          'isMotorDefective',
          'isSensorDefective',
          'isRunningTests',
          'isScanning',
          'isPreparing'
        ])
        this.write(this.commandArduino.RESTART)
        break
      default:
        this.write(this.commandArduino.STATUS)
        break
    }
  }

  private checkBlockingStatus(statusToCheck: StatusToCheck[]): void {
    let statusToError: string | null = null
    for (const status of statusToCheck) {
      if (this[status] === true) {
        statusToError = status
        break
      }
    }
    if (!statusToError) {
      return
    }

    switch (statusToError) {
      case 'isMotorDefective':
        throw new Error('Falha no motor. Tente desconectar e conectar novamente o scanner')
      case 'isSensorDefective':
        throw new Error('Falha no sensor. Tente desconectar e conectar novamente o scanner')
      case 'isRunningTests':
        throw new Error('Scanner fazendo testes no momento. Tente novamente mais tarde')
      case 'isPreparing':
        throw new Error(
          'Scanner está se preparando para fazer um novo escaneamento. Tente novamente mais tarde'
        )
      case 'isScanning':
        throw new Error('Scanner está escaneando. Tente novamente mais tarde')
      case 'isStopped':
        throw new Error('Scanner está parado. Tente fazer outro scan')
    }
  }

  private recieveResponseFromArduino(responseArduino: string): void {
    switch (responseArduino) {
      case this.responseArduino.TESTING:
        this.resetStatus()
        this.isRunningTests = true
        break
      case this.responseArduino.SCANNING:
        this.resetStatus()
        this.isScanning = true
        break
      case this.responseArduino.RESTARTING:
        this.resetStatus()
        this.isPreparing = true
        break
      case this.responseArduino.READY:
        this.resetStatus()
        break
      case this.responseArduino.ERROR_SENSOR:
        // Adicionar fazer alguma coisa aqui para mostrar o erro
        this.isSensorDefective = true
        break
      case this.responseArduino.ERROR_MOTOR:
        // Adicionar fazer alguma coisa aqui para mostrar o erro
        this.isMotorDefective = true
        break
      case this.responseArduino.STOPPED:
        // Tratar para estado parado?
        this.isStopped = true
        break
    }
  }

  private resetStatus(): void {
    this.isMotorDefective = false
    this.isSensorDefective = false
    this.isRunningTests = false
    this.isPreparing = false
    this.isScanning = false
    this.isStopped = false
  }

  public startScan(): void {
    this.sendCommandToArduino(this.commandArduino.SCAN)
  }

  public cancelScan(): void {
    this.sendCommandToArduino(this.commandArduino.CANCEL)
  }

  public stopScan(): void {
    this.sendCommandToArduino(this.commandArduino.STOP)
  }

  public testScan(): void {
    this.sendCommandToArduino(this.commandArduino.TEST)
  }

  public restartScan(): void {
    this.sendCommandToArduino(this.commandArduino.RESTART)
  }
}
