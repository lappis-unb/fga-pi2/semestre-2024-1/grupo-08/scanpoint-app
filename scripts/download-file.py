import shutil
import os

# Obtenha o diretório de execução atual
current_directory = os.path.dirname(os.path.abspath(__file__))


# Defina o nome do arquivo que deseja copiar (deve estar no diretório de execução atual)
file_name = 'cube.stl'

# Caminho completo do arquivo de origem
source_file = os.path.join(current_directory + '/stl/' , file_name)
print(f"Arquivo de origem: {source_file}")

# Obtenha o caminho da pasta de downloads do usuário
download_folder = os.path.join(os.path.expanduser("~"), "Downloads")

# Defina o caminho de destino
destination_file = os.path.join(download_folder, file_name)

# Copie o arquivo para a pasta de downloads
shutil.copy(source_file, destination_file)

print(f"Arquivo copiado para {destination_file}")
