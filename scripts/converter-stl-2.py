import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import uniform_filter

def pol2cart(theta, r, z):
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    return x, y, z

# Processing variables
maxDistance = 56  # Upper limit -- raw scan value only scanning "air"
minDistance = 24.5  # Lower limit -- raw scan value error: reporting negative reading

midThreshUpper = 0.5  # Offset radius threshold around 0
midThreshLower = -midThreshUpper  # Offset radius threshold around 0

windowSize = 3  # Window size for average filter to clean up mesh
interpRes = 1  # Interpolation resolution, i.e. keep every interRes-th row

centerDistance = 39  # [cm] - Distance from scanner to center of turntable
zDelta = 0.1
rawData = np.loadtxt('../data/farmer.txt')  # Load text file from SD Card
rawData[rawData < 0] = 0  # Remove erroneous scans from raw data

# indeces = np.where(rawData == 9999)[0]  # Find indices of '9999' delimiter in text file, indicating end of z-height scan

# Calcula o número de elementos a serem adicionados para que o total seja múltiplo de 200

# rawData = np.array(rawData)[rawData != 9999]

num_elements_to_add = 200 - (rawData.size % 200)

# Preenche o array com zeros
array_padded = np.pad(rawData, (0, num_elements_to_add), mode='constant')

# Reformata o array para uma matriz onde cada linha tem 200 elementos
matriz = array_padded.reshape(-1, 200)

matriz_transposta = matriz.T

# # Arrange into matrix, where each row corresponds to one z-height.
# r = [rawData[0:indeces[0]]]
# for i in range(1, len(indeces)):
#     r.append(rawData[indeces[i-1]+1:indeces[i]])

r = np.array(matriz_transposta)
r = np.delete(r, -1, axis=1)  # Delete last row of 9999 delimiters.

r = centerDistance - r  # Offset scan so that distance is with respect to turntable center of rotation

r[r > maxDistance] = np.nan  # Remove scan values greater than maxDistance
r[r < minDistance] = np.nan  # Remove scan values less than minDistance

# Remove scan values around 0
midThreshUpperIdx = r > midThreshLower
midThreshLowerIdx = r < midThreshUpper
midThreshIdx = midThreshUpperIdx * midThreshLowerIdx
r[midThreshIdx == 1] = np.nan

# Create theta matrix with the same size as r -- each column in r corresponds to specific orientation
theta = np.linspace(360, 0, r.shape[1], endpoint=False)
theta = np.tile(theta, (r.shape[0], 1))

theta = np.deg2rad(theta)  # Convert to radians

# Create z-height array where each row corresponds to one z-height
z = np.arange(0, r.shape[0] * zDelta, zDelta)
z = np.tile(z, (r.shape[1], 1)).T

x, y, z = pol2cart(theta, r, z)  # Convert to cartesian coordinates

# Replace NaN values in x, y with nearest neighbor at the same height
for i in range(x.shape[0]):
    if np.sum(np.isnan(x[i, :])) == x.shape[1]:
        x[i:, :] = []
        y[i:, :] = []
        z[i:, :] = []
        break

for i in range(x.shape[0]):
    latestValueIdx = np.where(~np.isnan(x[i, :]))[0][0]
    latestX = x[i, latestValueIdx]
    latestY = y[i, latestValueIdx]
    for j in range(x.shape[1]):
        if not np.isnan(x[i, j]):
            latestX = x[i, j]
            latestY = y[i, j]
        else:
            x[i, j] = latestX
            y[i, j] = latestY

# Resample array based on desired mesh resolution
interpIdx = np.arange(0, x.shape[0], interpRes)
xInterp = x[interpIdx, :]
yInterp = y[interpIdx, :]
zInterp = z[interpIdx, :]

# Smooth data to eliminate more noise
xInterp = np.pad(xInterp, ((0, 0), (windowSize, windowSize)), mode='symmetric')  # Add symmetric duplicate padding along rows to correctly filter array edges
yInterp = np.pad(yInterp, ((0, 0), (windowSize, windowSize)), mode='symmetric')  # Add symmetric duplicate padding along rows to correctly filter array edges
xInterp = uniform_filter(xInterp, size=(1, windowSize))  # Filter x
yInterp = uniform_filter(yInterp, size=(1, windowSize))  # Filter y
xInterp = xInterp[:, windowSize:-windowSize]  # Remove padding
yInterp = yInterp[:, windowSize:-windowSize]  # Remove padding

# Force scan to wrap by duplicating first column values at end of arrays
xInterp[:, -1] = xInterp[:, 0]
yInterp[:, -1] = yInterp[:, 0]
zInterp[:, -1] = zInterp[:, 0]

# Add top to close shape
xTop = np.mean(xInterp[-1, :])
yTop = np.mean(yInterp[-1, :])
xInterp = np.vstack([xInterp, np.full(xInterp.shape[1], xTop)])
yInterp = np.vstack([yInterp, np.full(yInterp.shape[1], yTop)])
zInterp = np.vstack([zInterp, np.full(zInterp.shape[1], zInterp[-1, 0] - zInterp[-2, 0] + zInterp[-1, 0])])

# # Plot point cloud as a mesh to verify that processing is correct
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.plot3D(xInterp.flatten(), yInterp.flatten(), zInterp.flatten(), '.b')
# plt.show()

# Export as STL file (requires numpy-stl library)
from stl import mesh

def surf2stl(filename, x, y, z):
    faces = []
    for i in range(x.shape[0] - 1):
        for j in range(x.shape[1] - 1):
            v0 = [x[i, j], y[i, j], z[i, j]]
            v1 = [x[i + 1, j], y[i + 1, j], z[i + 1, j]]
            v2 = [x[i, j + 1], y[i, j + 1], z[i, j + 1]]
            v3 = [x[i + 1, j + 1], y[i + 1, j + 1], z[i + 1, j + 1]]
            faces.append([v0, v1, v2])
            faces.append([v1, v3, v2])
    faces = np.array(faces)
    stl_mesh = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(faces):
        for j in range(3):
            stl_mesh.vectors[i][j] = f[j]
    stl_mesh.save(filename)

surf2stl('../resources/farmer.stl', xInterp, yInterp, zInterp)


